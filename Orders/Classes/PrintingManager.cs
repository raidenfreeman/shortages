﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using Zen.Barcode;
using BarcodeLib;

namespace Orders
{
    public class PrintingManager
    {
        readonly decimal totalCost;
        const int barcodeHeight = 23;
        const int barcodeLength = 100;

        const int rowHeight = 28;

        private const int xOffset = 15;
        private const int yOffset = 50;

        private readonly int items;
        private int itemIterator;
        private readonly int totalQuantity;

        //private PrintPreviewDialog previewDlg = null;
        private PrintDialog printDlg = null;
        private string[] stringArray;
        private Image[] barcodeArray;
        private Font printingFont;

        public PrintingManager(List<OrderedProduct> list)
        {
            totalQuantity = 0;
            items = list.Count;
            printingFont = new Font(new FontFamily("Courier New"), 9);
            barcodeArray = new Image[items];
            stringArray = new string[items];
            int i = 0;
            foreach (var p in list)
            {
                string s = p.Quantity.ToString().PadRight(5, ' ');
                stringArray[i] = s  + p.Code + ' ' + p.Name;
                if (stringArray[i].Length > 64)
                    stringArray[i] = stringArray[i].Remove(64);
                var a = new Barcode();
                try { barcodeArray[i] = a.Encode(TYPE.EAN13, p.Barcode, barcodeLength, barcodeHeight); }
                catch (Exception)
                { barcodeArray[i] = a.Encode(TYPE.CODE93, p.Barcode, barcodeLength + 60, barcodeHeight); }
                totalQuantity += p.Quantity;
                totalCost += p.Quantity * p.Price;
                i++;
            }
        }

        /*public void PrintPreviewAll()
        {
            //Create a PrintPreviewDialog object
            previewDlg = new PrintPreviewDialog();
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //Add print-page event handler
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            //Set Document property of PrintPreviewDialog
            previewDlg.Document = pd;
            //Display dialog
            previewDlg.Show();
        }*/
        public bool PrintAll()
        {
            //Create a PrintPreviewDialog object
            printDlg = new PrintDialog();
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //Add print-page event handler
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            printDlg.Document = pd;
            DialogResult result = printDlg.ShowDialog();
            if (result == DialogResult.OK)
                //Print
                pd.Print();
            else
                return false;
            return true;
        }
        public bool PrintAllAuto()
        {
            //Create a PrintDocument object
            PrintDocument pd = new PrintDocument();
            //Add print-page event handler
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            //Print
            pd.Print();
            return true;
        }

        public void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            int iteratorLastValue = itemIterator;
            float ypos = yOffset;
            float pageheight = ev.MarginBounds.Bottom;
            //float pageLength = 740;
            float pageLength = ev.PageBounds.Width;// - xOffset;
            float pageBound = pageLength - xOffset - 12;
            //Create a Graphics object
            Graphics g = ev.Graphics;

            SolidBrush blackBrush = new SolidBrush(Color.Black);

            Pen blackPen = new Pen(Color.Black, 1);

            Font header = new Font(new FontFamily("Courier New"), 12, FontStyle.Bold);
            g.DrawString("\t\t\t                     \t\t" + "           " + DateTime.Today.ToShortDateString(), header, blackBrush, xOffset, ypos - 2 * 17);
            g.DrawString("\t\t\t   ΔΕΛΤΙΟ ΠΑΡΑΓΓΕΛΙΑΣ\t\t" + Environment.UserDomainName.PadRight(10, ' ') + " " + DateTime.Now.ToShortTimeString(), header, blackBrush, xOffset, ypos - 17);
            g.DrawString("ΤΜΧ", printingFont, blackBrush, xOffset, ypos + 2);
            g.DrawString("ΕΙΔΟΣ", printingFont, blackBrush, xOffset + 250, ypos + 2);
            g.DrawString("ΥΠ.1", printingFont, blackBrush, xOffset + 520, ypos + 2);
            g.DrawString("ΥΠ.2", printingFont, blackBrush, xOffset + 575, ypos + 2);
            g.DrawString("BARCODE", printingFont, blackBrush, xOffset + 670, ypos + 2);

            g.DrawString("Απεστάλη:", header, blackBrush, xOffset, ypos - 34);
            g.DrawRectangle(blackPen, xOffset + 100, ypos - 35, 20, 20);

            g.DrawLine(blackPen, xOffset, ypos, pageBound - 2, ypos);

            ypos += 20;
            g.DrawLine(blackPen, xOffset, ypos, pageBound - 2, ypos);
            g.DrawLine(blackPen, xOffset, ypos - 2, pageBound - 2, ypos - 2);


            List<Tuple<int, float>> iteratorsToDrawLast = new List<Tuple<int, float>>();
            for (itemIterator = iteratorLastValue; itemIterator < items && ypos + rowHeight < pageheight; itemIterator++)
            {
                g.DrawString(stringArray[itemIterator], printingFont, blackBrush, xOffset, ypos + 5);
                //if the barcode is not EAN-13 and does not fit, mark it to draw it above all else
                if (barcodeArray[itemIterator].Size.Width > 100)
                    iteratorsToDrawLast.Add(new Tuple<int, float>(itemIterator, ypos));
                else //draw it normally if it's EAN-13
                    g.DrawImage(barcodeArray[itemIterator], xOffset + 650, ypos + 2);
                ypos += rowHeight;
                g.DrawLine(blackPen, xOffset, ypos, pageBound - 2, ypos);
            }
            g.DrawLine(blackPen, xOffset, yOffset, xOffset, ypos);
            g.DrawLine(blackPen, xOffset + 40, yOffset, xOffset + 40, ypos);
            g.DrawLine(blackPen, xOffset + 510, yOffset, xOffset + 510, ypos);
            g.DrawLine(blackPen, xOffset + 565, yOffset, xOffset + 565, ypos);
            g.DrawLine(blackPen, xOffset + 620, yOffset, xOffset + 620, ypos);
            g.DrawLine(blackPen, pageBound - 2, yOffset, pageBound - 2, ypos);

            //now draw the barcodes that are not EAN-13
            foreach (var itemToDraw in iteratorsToDrawLast)
                g.DrawImage(barcodeArray[itemToDraw.Item1], pageBound - barcodeArray[itemToDraw.Item1].Width - 2, itemToDraw.Item2);

            if (itemIterator < items || pageheight < ypos + 12)
                ev.HasMorePages = true;
            else
            {
                g.DrawRectangle(blackPen, xOffset, ypos, 180, 14);
                g.DrawRectangle(blackPen, xOffset + 180, ypos, 240, 14);
                g.DrawRectangle(blackPen, xOffset + 420, ypos, pageBound - xOffset - 420 - 2, 14);
                Font sumFont = new Font(new FontFamily("Courier New"), 9, FontStyle.Bold);
                g.DrawString("Σύνολο ειδών:   " + items.ToString().PadRight(5, ' ') + "   Σύνολο τεμαχίων:  " + totalQuantity.ToString().PadRight(10, ' ') + "\t\tΣύνολο Χονδρικής:  " + totalCost.ToString("0.00") + " \u20AC", sumFont, blackBrush, xOffset, ypos + 1);
            }
        }
    }
}
