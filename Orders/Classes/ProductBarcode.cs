﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace Orders
{
    [Table(Name = "APOTIKH_BARCODES")]
    public class ProductBarcode
    {
        private EntityRef<DatabaseProduct> _OrderableProduct;

        [Column(Name = "BRAP_AP_BARCODE", IsPrimaryKey = true, DbType = "VarChar(20) NOT NULL")]
        public string Barcode;

        [Column(Name = "BRAP_AP_ID", DbType = "Int")]
        public int Apothiki_ID;

        [Association(Storage = nameof(_OrderableProduct), ThisKey = nameof(Apothiki_ID))]
        public DatabaseProduct OrderableProduct
        {
            get { return this._OrderableProduct.Entity; }
            set { this._OrderableProduct.Entity = value; }
        }

        public ProductBarcode()
        {
            this._OrderableProduct = new EntityRef<DatabaseProduct>();
        }
    }
}
