﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace Orders.Classes
{
    [ProtoContract]
    public class Category
    {
        public Category(string s)
        {
            Value = s;
        }
        public Category()
        {
            Value = "";
        }
        [ProtoMember(1)]
        public string Value { get; set; }
    }
}
