﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orders.Classes;

namespace Orders
{
    [ProtoContract]
    public class OrderedProduct
    {
        [ProtoMember(1)]
        public string Name { get; set; }
        [ProtoMember(2)]
        public List<DateTime> AdditionTimes { get; private set; }
        [ProtoMember(3)]
        public string Barcode { get; set; }
        [ProtoMember(4)]
        public int Quantity { get; set; }
        [ProtoMember(5)]
        public decimal Price { get; set; }
        [ProtoMember(6)]
        public string Code { get; set; }
        [ProtoMember(7)]
        public string Category { get; set; }

        public bool IsSelected { get; set; }

        public decimal Total
        {
            get
            {
                return Price * Quantity;
            }
        }

        public DateTime AdditionTime
        {
            get
            {
                return AdditionTimes.LastOrDefault();
            }
            set
            {
                AdditionTimes.Add(value);
                Quantity++;
            }
        }

        public OrderedProduct()
        {
            AdditionTimes = new List<DateTime>();
        }

        public OrderedProduct(DateTime dt, string name, string barc, decimal price, string apCode)
        {
            AdditionTimes = new List<DateTime>();
            this.AdditionTime = dt;
            this.Name = name;
            this.Barcode = barc;
            this.Price = price;
            this.Code = apCode;
        }

    }
}
