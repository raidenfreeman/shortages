﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.IO;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using WindowsInput;
using Orders.Classes;
using ProtoBuf;

namespace Orders
{
    public partial class MainForm : Form
    {
        public const string MainSaveFile = "orderData.bin";
        public const string PrintedDir = "Printed";
        public const string BackupFile1 = "~orderData.bin";
        public const string BackupFile2 = "~orderData2.bin";
        public const int EAN13length = 13;
        public const string allCategories = "ΟΛΕΣ";


        public List<Category> categories;
        public List<OrderedProduct> orderList;
        public List<OrderedProduct> printedList;
        public List<OrderedProduct> displayedList;
        public decimal displayedListTotal;
        public int displayedListTotalTemaxia;

        private List<string> barcodesToLookupQueue;
        public MainForm()
        {
            InitializeComponent();
            orderList = new List<OrderedProduct>();
            printedList = new List<OrderedProduct>();
            displayedList = new List<OrderedProduct>();
            barcodesToLookupQueue = new List<string>();
            categories = new List<Category>();
            this.Size = Properties.Settings.Default.WindowSize;
            this.Location = Properties.Settings.Default.WindowLocation;
            this.WindowState = Properties.Settings.Default.WindowState;
        }

        private void αλλαγήΒάσηςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var SHIT = new NewChangeDB();
            SHIT.ShowDialog();
        }

        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        private void backgroundWorkerBarcodeFinder_DoWork(object sender, DoWorkEventArgs e)
        {
            string barcodeToLookup = e.Argument.ToString();

            string connectionString;
            if (Properties.Settings.Default.UseSQLAuth)
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + ";User Id=" + Properties.Settings.Default.DBUsername + ";Password=" + Properties.Settings.Default.DBPassword + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + ";";
            else
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + "; Integrated Security = True";
            DataContext db = new DataContext(connectionString);
            //Table<DatabaseProduct> Prod = db.GetTable<DatabaseProduct>().Where(x=>x.BarcodeS.Any(y=>y.Barcode == barcodeToLookup)).FirstOrDefault();
            //var q = from c in Prod where c.BarcodeS.Any(x => x.Barcode == barcodeToLookup) select c;
            //var result = q.FirstOrDefault();
            DatabaseProduct result = null;
            try { result = db.GetTable<DatabaseProduct>().Where(x => x.BarcodeS.Any(y => y.Barcode == barcodeToLookup)).FirstOrDefault(); }
            catch (Exception)
            { MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων."); }

            if (result != null)
            {
                var foundProduct = new OrderedProduct();
                foundProduct.AdditionTime = DateTime.Now;
                foundProduct.Name = result.Description + " " + result.Morfi;
                foundProduct.Barcode = barcodeToLookup;//result.BarcodeS.FirstOrDefault().Barcode;
                foundProduct.Price = result.TimiXondrikis;
                foundProduct.Code = result.KodikosApothikis;
                e.Result = foundProduct;
            }
            else
            {
                //look for a warehouse code
                result = db.GetTable<DatabaseProduct>().Where(x => x.KodikosApothikis == barcodeToLookup).FirstOrDefault();
                if (result != null)
                {
                    var foundProduct = new OrderedProduct();
                    foundProduct.AdditionTime = DateTime.Now;
                    foundProduct.Code = result.KodikosApothikis;
                    foundProduct.Name = result.Description + " " + result.Morfi;
                    var resBarcds = result.BarcodeS.FirstOrDefault();
                    if (resBarcds != null)
                        foundProduct.Barcode = resBarcds.Barcode;
                    else
                    {
                        foundProduct.Barcode = "0000000000000";
                        MessageBox.Show("ΠΡΟΣΟΧΉ: Το είδος δεν έχει barcode");
                    }
                    foundProduct.Price = result.TimiXondrikis;
                    e.Result = foundProduct;
                }
                else
                    e.Result = null;
            }
        }

        private void backgroundWorkerBarcodeFinder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("To είδος δεν βρέθηκε.");
                comboBox1.SelectAll();
                return;
            }
            AddFoundProduct((OrderedProduct)e.Result);
            comboBox1.Text = "";
        }

        private void AddFoundProduct(OrderedProduct tempProd)
        {
            var productInOrder = orderList.Find(x => x.Barcode == tempProd.Barcode);
            if (productInOrder != null)
            {
                productInOrder.AdditionTime = DateTime.Now;
                orderList = orderList.OrderBy(x => x.Barcode == productInOrder.Barcode).ToList();
            }
            else
            {
                orderList.Add(tempProd);
            }
            StartSaveThread();
            UpdateDataGrid(orderList);
            comboBox1.SelectedIndex = -1;// .Items.Clear(); comboBox1.Text = "";
            if (backgroundWorkerBarcodeFinder.IsBusy == false)
            {
                string rest = barcodesToLookupQueue.FirstOrDefault();
                if (rest != null)
                {
                    barcodesToLookupQueue.RemoveAt(0);
                    backgroundWorkerBarcodeFinder.RunWorkerAsync(rest);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*List<OrderedProduct> listToDisplay = new List<OrderedProduct>();
            DateTime from = dateTimePicker1.Value;
            DateTime to = dateTimePicker2.Value;
            if (from != to)
            {
                foreach (var orderedProd in orderList)
                {
                    var additionTimes = orderedProd.AdditionTimes.Where(x => x >= from && x <= to);
                    int temaxia = additionTimes.Count();
                    if (temaxia > 0)
                    {
                        var tempProduct = new OrderedProduct();
                        tempProduct.Barcode = orderedProd.Barcode;
                        tempProduct.Name = orderedProd.Name;
                        foreach (var additionTime in additionTimes)
                        {
                            tempProduct.AdditionTime = additionTime;
                        }
                        listToDisplay.Add(tempProduct);
                    }
                }
            }
            else
                listToDisplay = orderList;
            UpdateDataGrid(listToDisplay);*/
            UpdateDataGrid(orderList);
        }

        private void UpdateDataGrid(IEnumerable<OrderedProduct> list)
        {
            dataGridView1.Columns["Delete"].Visible = true;
            displayedListTotal = 0;
            displayedListTotalTemaxia = 0;
            foreach (var p in list)
            {
                displayedListTotal += p.Price * p.Quantity;
                displayedListTotalTemaxia += p.Quantity;
            }
            this.Invoke((MethodInvoker)delegate
            {
                displayedList = list.ToList();
                if (displayedList.Count != 0)
                {
                    displayedList.Reverse();
                    var sortableList = new SortableBindingList<OrderedProduct>(displayedList);
                    var source = new BindingSource(sortableList, null);
                    dataGridView1.DataSource = source;
                    dataGridView1.CurrentCell.Selected = false;
                }
                else
                    dataGridView1.DataSource = null;
                textBox2.Text = displayedListTotal.ToString("0.00") + " \u20AC";
                textBoxEidi.Text = displayedList.Count.ToString();
                textBoxTemaxia.Text = displayedListTotalTemaxia.ToString();
            });
        }

        private void LoadListFromSaves()
        {
            /*if (Properties.Settings.Default.LastSavedData.Date != DateTime.Today)
                return;*/
            string saveFile;
            //string date = DateTime.Today.ToString("_dd_MM_yyyy");
            if (File.Exists(MainSaveFile))
                saveFile = MainSaveFile;
            else
            {
                if (File.Exists(BackupFile1))
                    saveFile = BackupFile1;
                else if (File.Exists(BackupFile2))
                    saveFile = BackupFile2;
                else
                {
                    //MessageBox.Show("Δεν βρέθηκαν σημερινά αρχεία των δεδομένων.");
                    return;
                }
            }
            using (var file = File.OpenRead(saveFile))
            {
                orderList = ProtoBuf.Serializer.Deserialize<List<OrderedProduct>>(file);
            }
            if (orderList.Count > 0)
                UpdateDataGrid(orderList);
        }

        private void SaveToFile()
        {
            Properties.Settings.Default.Save();
            string mantissa = "temp";
            int i = 0;
            while (true)
            {
                if (File.Exists(MainSaveFile + mantissa))
                {
                    mantissa += i.ToString();
                    i++;
                }
                else
                    break;
            }
            try
            {
                using (var targetFile = File.Create(MainSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<OrderedProduct>>(targetFile, orderList);
                }
            }
            catch (IOException)
            {
                Thread.Sleep(10);
                mantissa += "exception";
                using (var targetFile = File.Create(MainSaveFile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<OrderedProduct>>(targetFile, orderList);
                }
            }
            try
            {
                File.Delete(MainSaveFile);
                File.Copy(MainSaveFile + mantissa, MainSaveFile);
                File.Delete(MainSaveFile + mantissa);
            }
            catch (IOException)
            {
                Thread.Sleep(20);
                File.Copy(MainSaveFile + mantissa, MainSaveFile);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            dataGridView1.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2;
            backgroundWorkerLoad.RunWorkerAsync();
            if (Properties.Settings.Default.Installed == false)
            {
                InstallationForm n = new InstallationForm();
                n.ShowDialog();
            }

            if (File.Exists("categories.phbin"))
                using (var file = File.OpenRead("categories.phbin"))
                {
                    categories = Serializer.Deserialize<List<Category>>(file);
                }
            else
                categories = new List<Classes.Category>();

            
            ((DataGridViewComboBoxColumn)dataGridView1.Columns["Category"]).DataSource = categories.Select(x => x.Value).ToList();
            var categoryDataSource = categories.Select(x => x.Value).ToList();
            categoryDataSource.Insert(0, allCategories);
            comboBoxCategory.DataSource = categoryDataSource;
            //((DataGridViewComboBoxColumn)dataGridView1.Columns["Category"]).DisplayMember = "Value";

            #region getting all products

            string connectionString;
            if (Properties.Settings.Default.UseSQLAuth)
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + ";User Id=" + Properties.Settings.Default.DBUsername + ";Password=" + Properties.Settings.Default.DBPassword + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + ";";
            else
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + "; Integrated Security = True";
            DataContext db = new DataContext(connectionString);
            List<DatabaseProduct> result = new List<DatabaseProduct>();
            try { result = db.GetTable<DatabaseProduct>().OrderBy(x => x.Description).ToList(); }
            catch (Exception)
            {
                MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων.");
            }
            comboBox1.DataSource = result;
            comboBox1.DisplayMember = "FullName";
            comboBox1.SelectedIndex = -1;

            #endregion

        }

        private void backgroundWorkerSave_DoWork(object sender, DoWorkEventArgs e)
        {
            SaveToFile();
        }

        private void backgroundWorkerLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadListFromSaves();
        }

        private void εκτύπωσηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (displayedList.Count == 0)
                return;
            PrintingManager p = new PrintingManager(displayedList.OrderBy(x => x.Name).ToList());
            p.PrintAll();
            /*if (p.PrintAll())
            {
                printedList = displayedList;
                RemovePrintedItems();
                displayedList.Clear();
                UpdateDataGrid(displayedList);
            }*/

        }

        private void RemovePrintedItems()
        {
            orderList = orderList.Except(printedList).ToList();
            StartSaveThread();
            if (dataGridView1.Columns["Delete"].Visible == true)
            {
                if (!Directory.Exists(PrintedDir))
                    Directory.CreateDirectory(PrintedDir);
                string date = DateTime.Today.ToString("dd_MM_yyyy_");
                string mantissa = "temp";
                string savefile = PrintedDir + '\\' + date + MainSaveFile;
                int i = 0;
                while (true)
                {
                    if (File.Exists(savefile))
                    {
                        i++;
                        savefile = PrintedDir + '\\' + date + MainSaveFile + '(' + (i + 1).ToString() + ')';
                    }
                    else
                        break;
                }
                using (var targetFile = File.Create(savefile + mantissa))
                {
                    ProtoBuf.Serializer.Serialize<List<OrderedProduct>>(targetFile, printedList);
                }
                File.Delete(savefile);
                File.Copy(savefile + mantissa, savefile);
                File.Delete(savefile + mantissa);
            }
        }

        private void αποθήκευσηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StartSaveThread();
        }

        private void φόρτωσηToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            backgroundWorkerLoad.RunWorkerAsync();
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                displayedListTotal = 0;
                displayedListTotalTemaxia = 0;
                foreach (var p in orderList)
                {
                    displayedListTotalTemaxia += p.Quantity;
                    displayedListTotal += p.Price * p.Quantity;
                }
                textBox2.Text = displayedListTotal.ToString("0.00") + " \u20AC";
                textBoxTemaxia.Text = displayedListTotalTemaxia.ToString();
                textBoxEidi.Text = displayedList.Count.ToString();
                StartSaveThread();
            }

        }

        private void εκτύπωσηΣεΠροεπιλογήToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (displayedList.Count == 0)
                return;
            PrintingManager p = new PrintingManager(displayedList.OrderBy(x => x.Name).ToList());
            p.PrintAllAuto();
            /*if (p.PrintAllAuto())
            {
                printedList = displayedList;
                RemovePrintedItems();
                displayedList.Clear();
                UpdateDataGrid(displayedList);
            }*/
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Category"].Index)
            {
                dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "";
                e.Cancel = true;
            }
            else
                MessageBox.Show("Η ποσότητα πρέπει να είναι μεταξύ 1 και 99999");
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            string text = comboBox1.Text;
            /*if (text.Length == 2)
            {
                
            }*/
            if (e.KeyCode == Keys.Enter)
            {
                if (System.Text.RegularExpressions.Regex.Matches(text, @"[a-zA-Z]").Count == 0)
                {
                    if (backgroundWorkerBarcodeFinder.IsBusy)
                    {
                        BackgroundWorker backgroundCodeFinder = new BackgroundWorker();
                        backgroundCodeFinder.DoWork += backgroundWorkerBarcodeFinder_DoWork;
                        backgroundCodeFinder.RunWorkerCompleted += backgroundWorkerBarcodeFinder_RunWorkerCompleted;
                        backgroundCodeFinder.RunWorkerAsync(text);
                    }
                    else
                        backgroundWorkerBarcodeFinder.RunWorkerAsync(text);
                }
                else
                {
                    var item = (DatabaseProduct)comboBox1.SelectedItem;
                    if (item != null)
                    {
                        var foundProduct = new OrderedProduct();
                        foundProduct.AdditionTime = DateTime.Now;
                        foundProduct.Name = item.Description + " " + item.Morfi;
                        var barcodes = item.BarcodeS.FirstOrDefault();
                        if (barcodes != null)
                            foundProduct.Barcode = barcodes.Barcode;//result.BarcodeS.FirstOrDefault().Barcode;
                        else
                        {
                            foundProduct.Barcode = "0000000000000";
                            MessageBox.Show("ΠΡΟΣΟΧΉ: Το είδος δεν έχει barcode");
                        }
                        foundProduct.Price = item.TimiXondrikis;
                        foundProduct.Code = item.KodikosApothikis;
                        AddFoundProduct(foundProduct);
                    }
                }
                /*
            else
            {
                if (backgroundWorkerNameFinder.IsBusy)
                {
                    BackgroundWorker backgroundNameFinder = new BackgroundWorker();
                    backgroundNameFinder.DoWork += backgroundWorkerNameFinder_DoWork;
                    backgroundNameFinder.RunWorkerCompleted += backgroundWorkerNameFinder_RunWorkerCompleted;
                    backgroundNameFinder.RunWorkerAsync(text);
                }
                else
                    backgroundWorkerNameFinder.RunWorkerAsync(text);
            }*/
                e.SuppressKeyPress = true;
            }
        }

        void StartSaveThread()
        {
            if (backgroundWorkerSave.IsBusy)
            {
                BackgroundWorker newBackgroundWorkerSave = new BackgroundWorker();
                newBackgroundWorkerSave.DoWork += backgroundWorkerSave_DoWork;
                newBackgroundWorkerSave.RunWorkerAsync();
            }
            else
                backgroundWorkerSave.RunWorkerAsync();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if ((senderGrid.Columns[e.ColumnIndex].Name == "Delete") && e.RowIndex >= 0)
            {
                orderList.Remove((OrderedProduct)senderGrid.Rows[e.RowIndex].DataBoundItem);
                UpdateDataGrid(orderList);
                StartSaveThread();
            }
        }

        private void εγκατάστασηToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstallationForm n = new InstallationForm();
            n.ShowDialog();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.WindowState = this.WindowState;
            if (this.WindowState == FormWindowState.Normal)
                Properties.Settings.Default.WindowSize = this.Size;
            else
                Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.Save();
        }

        private void αποστολήΣεFarmakonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //System.Timers.Timer tim = new System.Timers.Timer(10);
            //
            /*
            Process farnet = Process.GetProcessById(7788); //.GetProcessesByName("notepad++");
            if (farnet == null)
            {
                MessageBox.Show("Δεν βρέθηκε το FarmakoNet SQL");
                return;
            }
            IntPtr farmakonHandle = farnet.Handle;
            //IntPtr farmakonHandle = FindWindow(null, "Notepad++");
            Process currentProcess = Process.GetCurrentProcess();
            IntPtr currentProcessHandle = currentProcess.MainWindowHandle;
            if (farmakonHandle == IntPtr.Zero)
            {
                MessageBox.Show("Δεν βρέθηκε το handle του Farmakon.NET SQL");
                return;
            }
            SetForegroundWindow(farmakonHandle);*/
            if (dataGridView1.IsCurrentRowDirty)
                dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            var listToSend = displayedList.Where(x => x.IsSelected == true).ToList();
            int lim = listToSend.Count;

            /*InputSimulator inputSimulator = new InputSimulator();
            inputSimulator.Keyboard.ModifiedKeyStroke(WindowsInput.Native.VirtualKeyCode.MENU, WindowsInput.Native.VirtualKeyCode.TAB);
            Thread.Sleep(100);*/
            SendKeys.SendWait("%{TAB}");
            Thread.Sleep(100);
            for (int j = 0; j < lim; j++)//foreach (var P in listToSend)
            {
                for (int i = 0; i < listToSend[j].Quantity; i++)
                {
                    /*inputSimulator.Keyboard.TextEntry(listToSend[j].Barcode + '\n');
                    if (timeDelay > 0)
                        Thread.Sleep(timeDelay);*/
                    SendKeys.SendWait(listToSend[j].Barcode + "{ENTER}");
                    Thread.Sleep(100);
                    SendKeys.SendWait("{ENTER}");
                    Thread.Sleep(100);
                }
            }

            //printedList = listToSend;
            //RemovePrintedItems();
            //displayedList = displayedList.Except(listToSend).ToList();// .Clear();
            //UpdateDataGrid(displayedList);
            /*
            foreach(OrderedProduct p in displayedList)
            {
                SendKeys.SendWait(p.Barcode);
                System.Threading.Thread.Sleep(10);
                SendKeys.SendWait("{ENTER}");
                System.Threading.Thread.Sleep(10);
            }*/
            //SendKeys.SendWait("{F3}");
            //SetForegroundWindow(currentProcessHandle);
            //MessageBox.Show("Ολοκληρώθηκε!"+farnet[0].MainWindowTitle);
        }

        private void backgroundWorkerNameFinder_DoWork(object sender, DoWorkEventArgs e)
        {
            string nameToLookup = e.Argument.ToString();

            string connectionString;
            if (Properties.Settings.Default.UseSQLAuth)
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + ";User Id=" + Properties.Settings.Default.DBUsername + ";Password=" + Properties.Settings.Default.DBPassword + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + ";";
            else
                connectionString = "Data Source = " + Properties.Settings.Default.DatabaseName + "; Initial Catalog = " + Properties.Settings.Default.DatabaseCatalogName + "; Integrated Security = True";
            DataContext db = new DataContext(connectionString);
            List<DatabaseProduct> result = new List<DatabaseProduct>();
            try { result = db.GetTable<DatabaseProduct>().Where(x => x.Description.StartsWith(nameToLookup)).ToList(); }
            catch (Exception)
            { MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων."); }

            if (result.Count() > 0)
            {
                var foundProducts = new List<OrderedProduct>();
                foreach (var prod in result)
                {
                    var newProd = new OrderedProduct();
                    newProd.AdditionTime = DateTime.Now;
                    newProd.Name = prod.Description + " " + prod.Morfi;
                    var a = prod.BarcodeS.FirstOrDefault();
                    if (a != null)
                        newProd.Barcode = a.Barcode;
                    else
                        continue;
                    newProd.Price = prod.TimiXondrikis;
                    newProd.Code = prod.KodikosApothikis;
                    foundProducts.Add(newProd);
                }
                e.Result = foundProducts;
            }
            else
            {
                try { result = db.GetTable<DatabaseProduct>().Where(x => x.BarcodeS.Any(y => y.Barcode == nameToLookup)).ToList(); }
                catch (Exception)
                { MessageBox.Show("Βεβαιωθείτε πως η ασφάλεια της βάσης δέχεται SQL Server Authentication εκτός από Windows Authentication.\nΕπίσης, βεβαιωθείτε ότι το όνομα χρήστη " + Properties.Settings.Default.DBUsername + " υπάρχει στη βάση δεδομένων, και έχει δικαίωμα ανάγνωσης.", "Σφάλμα πρόσβασης στη βάση δεδομένων."); }

                if (result != null)
                {
                    var tempProd = result.FirstOrDefault();
                    if (tempProd != null)
                    {
                        var foundProduct = new OrderedProduct();
                        foundProduct.AdditionTime = DateTime.Now;
                        foundProduct.Name = tempProd.Description + " " + tempProd.Morfi;
                        foundProduct.Barcode = nameToLookup;
                        foundProduct.Price = tempProd.TimiXondrikis;
                        foundProduct.Code = tempProd.KodikosApothikis;
                        var tempList = new List<OrderedProduct>();
                        tempList.Add(foundProduct);
                        e.Result = tempList;
                    }
                    else
                        e.Result = null;
                }
                else
                    e.Result = null;
            }
        }

        private void backgroundWorkerNameFinder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result == null)
            {
                MessageBox.Show("To είδος δεν βρέθηκε!");
                comboBox1.SelectAll();
                return;
            }
            comboBox1.DataSource = ((List<OrderedProduct>)e.Result).Select(x => x.Name).ToList();
            /*List<OrderedProduct> tempList = (List<OrderedProduct>)e.Result;
            if (tempList.Count == 1)
            {
                orderList.Add(tempList.First());
                UpdateDataGrid(orderList);
                comboBox1.Items.Clear();
                comboBox1.Text = "";
                var productInOrder = orderList.Find(x => x.Barcode == tempList.First().Barcode);
                if (productInOrder != null)
                {
                    productInOrder.AdditionTime = DateTime.Now;
                    orderList = orderList.OrderBy(x => x.Barcode == productInOrder.Barcode).ToList();
                }
                else
                {
                    orderList.Add(tempList.First());
                }
                StartSaveThread();
                UpdateDataGrid(orderList);
                comboBox1.Items.Clear();
                comboBox1.Text = "";
                if (backgroundWorkerBarcodeFinder.IsBusy == false)
                {
                    string rest = barcodesToLookupQueue.FirstOrDefault();
                    if (rest != null)
                    {
                        barcodesToLookupQueue.RemoveAt(0);
                        backgroundWorkerBarcodeFinder.RunWorkerAsync(rest);
                    }
                }
            }
            else
            {
                comboBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                comboBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;
                var autoCompleteList = new AutoCompleteStringCollection();
                autoCompleteList.AddRange(tempList.Select(x => x.Name).ToArray());
                comboBox1.AutoCompleteCustomSource = autoCompleteList;
            }*/
        }

        private void εκτυπωμένεςΠαραγγελίεςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(PrintedDir))
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.InitialDirectory = Path.Combine(Directory.GetCurrentDirectory(), PrintedDir);
                DialogResult res = fd.ShowDialog();
                if (res == DialogResult.OK)
                {
                    using (var file = fd.OpenFile())
                    {
                        printedList = ProtoBuf.Serializer.Deserialize<List<OrderedProduct>>(file);
                    }
                    UpdateDataGrid(printedList);
                    dataGridView1.Columns["Delete"].Visible = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (var p in displayedList)
                p.IsSelected = true;
            displayedList.Reverse();
            UpdateDataGrid(displayedList);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (var p in displayedList)
                p.IsSelected = false;
            displayedList.Reverse();
            UpdateDataGrid(displayedList);
        }

        private void προσθήκηΧρονοκαθυστέρησηςToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Orders.Forms.CategoryManagement dialog = new Forms.CategoryManagement();
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                categories = dialog.categories.ToList<Category>();

                var categoryDataSource = categories.Select(x => x.Value).ToList();
                ((DataGridViewComboBoxColumn)dataGridView1.Columns["Category"]).DataSource = categories.Select(x => x.Value).ToList();
                categoryDataSource.Insert(0, allCategories);
                comboBoxCategory.DataSource = categoryDataSource;
            }

            /*
            if (timeDelay == 0)
            {
                timeDelay = 100;
                MessageBox.Show("Προστέθηκε χρονοκαθυστέρηση 100 millisecond.");
                timeDelayToolStripMenuItem.Text = "Αφαίρεση Χρονοκαθυστέρησης";
            }
            else
            {
                timeDelay = 0;
                MessageBox.Show("Αφαιρέθηκε η χρονοκαθυστέρηση.");
                timeDelayToolStripMenuItem.Text = "Προσθήκη Χρονοκαθυστέρησης";
            }*/
        }

        private void διαγραφήΕπιλεγμένωνToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<OrderedProduct> printedList = new List<OrderedProduct>();
            foreach (var p in displayedList.Where(x => x.IsSelected == true))
                printedList.Add(p);
            RemovePrintedItems();
            displayedList = displayedList.Except(printedList).ToList();// .Clear();
            UpdateDataGrid(displayedList);
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {

        }

        void ctl_Enter(object sender, EventArgs e)
        {
            (sender as ComboBox).DroppedDown = true;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bool validRow = (e.RowIndex != -1); //Make sure the clicked row isn't the header.
            var datagridview = sender as DataGridView;

            // Check to make sure the cell clicked is the cell containing the combobox 
            if (datagridview.Columns[e.ColumnIndex] is DataGridViewComboBoxColumn && validRow)
            {
                datagridview.BeginEdit(true);
                ((ComboBox)datagridview.EditingControl).DroppedDown = true;
            }
        }

        private void comboBoxCategory_SelectedValueChanged(object sender, EventArgs e)
        {
            var box = (ComboBox)sender;
            if ((string)box.SelectedValue == allCategories)
                UpdateDataGrid(orderList);
            else
                UpdateDataGrid(orderList.Where(x => x.Category == (string)box.SelectedValue).ToList());
        }
    }
}
