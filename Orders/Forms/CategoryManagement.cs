﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProtoBuf;
using System.IO;
using Orders.Classes;
using System.Threading;

namespace Orders.Forms
{
    public partial class CategoryManagement : Form
    {
        string categoriesFile = "categories.phbin";
        //XmlSerializer serializer;
        public BindingList<Category> categories;

        public CategoryManagement(BindingList<Category> categoriesInput = null)
        {
            InitializeComponent();

            if (categoriesInput == null)
            {
                if (File.Exists(categoriesFile))
                    using (var file = File.OpenRead(categoriesFile))
                    {
                        categories = new BindingList<Category>((Serializer.Deserialize<List<Category>>(file)));
                    }
                else
                    categories = new BindingList<Category>();
            }
            else
                categories = categoriesInput;

            BindingSource source = new BindingSource();
            source.DataSource = categories;
            dataGridView1.DataSource = source;
            dataGridView1.Columns[0].HeaderText = "Όνομα Κατηγορίας";
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            categories.Add(new Category());
            dataGridView1.ClearSelection();
            dataGridView1.CurrentCell = dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[0];
            dataGridView1.BeginEdit(true);
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            categories.Remove((Category)dataGridView1.CurrentRow.DataBoundItem);
        }

        private void CategoryManagement_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (categories.FirstOrDefault() == null)
                File.Delete(categoriesFile);
            else
            {
                using (var file = File.Create(categoriesFile + "temp"))
                {
                    Serializer.Serialize(file, categories.ToList());
                }
                try
                {
                    File.Delete(categoriesFile);
                    File.Copy(categoriesFile + "temp", categoriesFile);
                    File.Delete(categoriesFile + "temp");
                }
                catch (IOException)
                {
                    Thread.Sleep(20);
                    File.Copy(categoriesFile + "temp", categoriesFile);
                }
            }
            this.DialogResult = DialogResult.OK;
        }
    }
}
