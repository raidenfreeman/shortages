﻿namespace Orders
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.αποθήκευσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.φόρτωσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αλλαγήΒάσηςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτύπωσηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εγκατάστασηToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.αποστολήΣεFarmakonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeDelayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.διαγραφήΕπιλεγμένωνToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.buttonHidePrinted = new System.Windows.Forms.Button();
            this.labelBarcode = new System.Windows.Forms.Label();
            this.backgroundWorkerBarcodeFinder = new System.ComponentModel.BackgroundWorker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Category = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsSelected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.orderedProductBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.backgroundWorkerSave = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerLoad = new System.ComponentModel.BackgroundWorker();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxCategory = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxEidi = new System.Windows.Forms.TextBox();
            this.textBoxTemaxia = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.backgroundWorkerNameFinder = new System.ComponentModel.BackgroundWorker();
            this.additionTimesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridViewComboBoxColumn1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.orderableProductBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.orderableProductBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderedProductBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.additionTimesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.αποθήκευσηToolStripMenuItem,
            this.φόρτωσηToolStripMenuItem,
            this.αλλαγήΒάσηςToolStripMenuItem,
            this.εκτύπωσηToolStripMenuItem,
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem,
            this.εγκατάστασηToolStripMenuItem,
            this.αποστολήΣεFarmakonToolStripMenuItem,
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem,
            this.timeDelayToolStripMenuItem,
            this.διαγραφήΕπιλεγμένωνToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(10, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1330, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // αποθήκευσηToolStripMenuItem
            // 
            this.αποθήκευσηToolStripMenuItem.Name = "αποθήκευσηToolStripMenuItem";
            this.αποθήκευσηToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.αποθήκευσηToolStripMenuItem.Text = "Αποθήκευση";
            this.αποθήκευσηToolStripMenuItem.Click += new System.EventHandler(this.αποθήκευσηToolStripMenuItem_Click);
            // 
            // φόρτωσηToolStripMenuItem
            // 
            this.φόρτωσηToolStripMenuItem.Name = "φόρτωσηToolStripMenuItem";
            this.φόρτωσηToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.φόρτωσηToolStripMenuItem.Text = "Φόρτωση";
            this.φόρτωσηToolStripMenuItem.Click += new System.EventHandler(this.φόρτωσηToolStripMenuItem_Click_1);
            // 
            // αλλαγήΒάσηςToolStripMenuItem
            // 
            this.αλλαγήΒάσηςToolStripMenuItem.Name = "αλλαγήΒάσηςToolStripMenuItem";
            this.αλλαγήΒάσηςToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.αλλαγήΒάσηςToolStripMenuItem.Text = "Αλλαγή Βάσης";
            this.αλλαγήΒάσηςToolStripMenuItem.Click += new System.EventHandler(this.αλλαγήΒάσηςToolStripMenuItem_Click);
            // 
            // εκτύπωσηToolStripMenuItem
            // 
            this.εκτύπωσηToolStripMenuItem.Name = "εκτύπωσηToolStripMenuItem";
            this.εκτύπωσηToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.εκτύπωσηToolStripMenuItem.Text = "Εκτύπωση";
            this.εκτύπωσηToolStripMenuItem.Click += new System.EventHandler(this.εκτύπωσηToolStripMenuItem_Click);
            // 
            // εκτύπωσηΣεΠροεπιλογήToolStripMenuItem
            // 
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Name = "εκτύπωσηΣεΠροεπιλογήToolStripMenuItem";
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Size = new System.Drawing.Size(159, 20);
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Text = "Εκτύπωση Σε Προεπιλογή";
            this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem.Click += new System.EventHandler(this.εκτύπωσηΣεΠροεπιλογήToolStripMenuItem_Click);
            // 
            // εγκατάστασηToolStripMenuItem
            // 
            this.εγκατάστασηToolStripMenuItem.Name = "εγκατάστασηToolStripMenuItem";
            this.εγκατάστασηToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.εγκατάστασηToolStripMenuItem.Text = "Εγκατάσταση";
            this.εγκατάστασηToolStripMenuItem.Click += new System.EventHandler(this.εγκατάστασηToolStripMenuItem_Click);
            // 
            // αποστολήΣεFarmakonToolStripMenuItem
            // 
            this.αποστολήΣεFarmakonToolStripMenuItem.Name = "αποστολήΣεFarmakonToolStripMenuItem";
            this.αποστολήΣεFarmakonToolStripMenuItem.Size = new System.Drawing.Size(146, 20);
            this.αποστολήΣεFarmakonToolStripMenuItem.Text = "Αποστολή σε Farmakon";
            this.αποστολήΣεFarmakonToolStripMenuItem.Click += new System.EventHandler(this.αποστολήΣεFarmakonToolStripMenuItem_Click);
            // 
            // εκτυπωμένεςΠαραγγελίεςToolStripMenuItem
            // 
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Name = "εκτυπωμένεςΠαραγγελίεςToolStripMenuItem";
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Size = new System.Drawing.Size(162, 20);
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Text = "Εκτυπωμένες Παραγγελίες";
            this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem.Click += new System.EventHandler(this.εκτυπωμένεςΠαραγγελίεςToolStripMenuItem_Click);
            // 
            // timeDelayToolStripMenuItem
            // 
            this.timeDelayToolStripMenuItem.Name = "timeDelayToolStripMenuItem";
            this.timeDelayToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.timeDelayToolStripMenuItem.Text = "Κατηγορίες";
            this.timeDelayToolStripMenuItem.Click += new System.EventHandler(this.προσθήκηΧρονοκαθυστέρησηςToolStripMenuItem_Click);
            // 
            // διαγραφήΕπιλεγμένωνToolStripMenuItem
            // 
            this.διαγραφήΕπιλεγμένωνToolStripMenuItem.Name = "διαγραφήΕπιλεγμένωνToolStripMenuItem";
            this.διαγραφήΕπιλεγμένωνToolStripMenuItem.Size = new System.Drawing.Size(145, 20);
            this.διαγραφήΕπιλεγμένωνToolStripMenuItem.Text = "Διαγραφή Επιλεγμένων";
            this.διαγραφήΕπιλεγμένωνToolStripMenuItem.Click += new System.EventHandler(this.διαγραφήΕπιλεγμένωνToolStripMenuItem_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "Από: dd/MM/yy     hh:mm";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(1114, 684);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(5);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(263, 32);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.TabStop = false;
            this.dateTimePicker1.Visible = false;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(648, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 111);
            this.button1.TabIndex = 4;
            this.button1.TabStop = false;
            this.button1.Text = "Προβολή πωλήσεων";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "Εώς: dd/MM/yy     hh:mm";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(1021, 590);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(267, 32);
            this.dateTimePicker2.TabIndex = 5;
            this.dateTimePicker2.TabStop = false;
            this.dateTimePicker2.Visible = false;
            // 
            // buttonHidePrinted
            // 
            this.buttonHidePrinted.Enabled = false;
            this.buttonHidePrinted.Location = new System.Drawing.Point(914, 639);
            this.buttonHidePrinted.Margin = new System.Windows.Forms.Padding(10);
            this.buttonHidePrinted.Name = "buttonHidePrinted";
            this.buttonHidePrinted.Size = new System.Drawing.Size(171, 72);
            this.buttonHidePrinted.TabIndex = 6;
            this.buttonHidePrinted.TabStop = false;
            this.buttonHidePrinted.Text = "Απόκρυψη Εκτυπωμένων";
            this.buttonHidePrinted.UseVisualStyleBackColor = true;
            this.buttonHidePrinted.Visible = false;
            // 
            // labelBarcode
            // 
            this.labelBarcode.AutoSize = true;
            this.labelBarcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBarcode.Location = new System.Drawing.Point(3, 0);
            this.labelBarcode.Name = "labelBarcode";
            this.labelBarcode.Size = new System.Drawing.Size(245, 53);
            this.labelBarcode.TabIndex = 7;
            this.labelBarcode.Text = "Barcode";
            this.labelBarcode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // backgroundWorkerBarcodeFinder
            // 
            this.backgroundWorkerBarcodeFinder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerBarcodeFinder_DoWork);
            this.backgroundWorkerBarcodeFinder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerBarcodeFinder_RunWorkerCompleted);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.quantityDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.Category,
            this.Price,
            this.Total,
            this.barcodeDataGridViewTextBoxColumn,
            this.IsSelected,
            this.Delete});
            this.dataGridView1.DataSource = this.orderedProductBindingSource;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightGreen;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView1.Location = new System.Drawing.Point(0, 163);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1330, 567);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.quantityDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.quantityDataGridViewTextBoxColumn.HeaderText = "ΤΕΜΑΧΙΑ";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            this.quantityDataGridViewTextBoxColumn.Width = 104;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "ΠΕΡΙΓΡΑΦΗ";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn.Width = 120;
            // 
            // Category
            // 
            this.Category.DataPropertyName = "Category";
            this.Category.HeaderText = "ΚΑΤΗΓΟΡΙΑ";
            this.Category.Name = "Category";
            this.Category.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Category.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Category.Width = 122;
            // 
            // Price
            // 
            this.Price.DataPropertyName = "Price";
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = null;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Price.DefaultCellStyle = dataGridViewCellStyle4;
            this.Price.HeaderText = "ΤΙΜΗ ΜΟΝΑΔΑΣ";
            this.Price.Name = "Price";
            this.Price.ReadOnly = true;
            this.Price.Width = 161;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            dataGridViewCellStyle5.Format = "C2";
            dataGridViewCellStyle5.NullValue = null;
            this.Total.DefaultCellStyle = dataGridViewCellStyle5;
            this.Total.HeaderText = "ΣΥΝΟΛΟ";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // barcodeDataGridViewTextBoxColumn
            // 
            this.barcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode";
            this.barcodeDataGridViewTextBoxColumn.HeaderText = "BARCODE";
            this.barcodeDataGridViewTextBoxColumn.Name = "barcodeDataGridViewTextBoxColumn";
            this.barcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.barcodeDataGridViewTextBoxColumn.Width = 108;
            // 
            // IsSelected
            // 
            this.IsSelected.DataPropertyName = "IsSelected";
            this.IsSelected.HeaderText = "ΑΠΟΣΤΟΛΗ ΣΕ FARMAKON";
            this.IsSelected.Name = "IsSelected";
            this.IsSelected.Width = 222;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "ΔΙΑΓΡΑΦΗ";
            this.Delete.Name = "Delete";
            this.Delete.Width = 93;
            // 
            // orderedProductBindingSource
            // 
            this.orderedProductBindingSource.DataSource = typeof(Orders.OrderedProduct);
            // 
            // backgroundWorkerSave
            // 
            this.backgroundWorkerSave.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerSave_DoWork);
            // 
            // backgroundWorkerLoad
            // 
            this.backgroundWorkerLoad.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerLoad_DoWork);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.DodgerBlue;
            this.textBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(256, 58);
            this.textBox2.Margin = new System.Windows.Forms.Padding(5);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(171, 32);
            this.textBox2.TabIndex = 1;
            this.textBox2.TabStop = false;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Lucida Sans Unicode", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(254, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 53);
            this.label1.TabIndex = 10;
            this.label1.Text = "Σύνολο Χονδρικής";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.66667F));
            this.tableLayoutPanel1.Controls.Add(this.comboBox1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelBarcode, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox2, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 2);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(432, 127);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(3, 56);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(245, 28);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.AutoScroll = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 183F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 4, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 28);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1330, 131);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.comboBoxCategory, 0, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(441, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(194, 100);
            this.tableLayoutPanel5.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Επιλογή Κατηγορίας";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxCategory
            // 
            this.comboBoxCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxCategory.CausesValidation = false;
            this.comboBoxCategory.FormattingEnabled = true;
            this.comboBoxCategory.Location = new System.Drawing.Point(3, 61);
            this.comboBoxCategory.Name = "comboBoxCategory";
            this.comboBoxCategory.Size = new System.Drawing.Size(188, 28);
            this.comboBoxCategory.TabIndex = 1;
            this.comboBoxCategory.SelectedValueChanged += new System.EventHandler(this.comboBoxCategory_SelectedValueChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.textBoxEidi, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.textBoxTemaxia, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(811, 21);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.27273F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.72727F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(342, 88);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // textBoxEidi
            // 
            this.textBoxEidi.BackColor = System.Drawing.Color.DodgerBlue;
            this.textBoxEidi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEidi.ForeColor = System.Drawing.Color.White;
            this.textBoxEidi.Location = new System.Drawing.Point(176, 51);
            this.textBoxEidi.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxEidi.Name = "textBoxEidi";
            this.textBoxEidi.ReadOnly = true;
            this.textBoxEidi.Size = new System.Drawing.Size(161, 32);
            this.textBoxEidi.TabIndex = 3;
            this.textBoxEidi.TabStop = false;
            this.textBoxEidi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxTemaxia
            // 
            this.textBoxTemaxia.BackColor = System.Drawing.Color.DodgerBlue;
            this.textBoxTemaxia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxTemaxia.ForeColor = System.Drawing.Color.White;
            this.textBoxTemaxia.Location = new System.Drawing.Point(5, 51);
            this.textBoxTemaxia.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxTemaxia.Name = "textBoxTemaxia";
            this.textBoxTemaxia.ReadOnly = true;
            this.textBoxTemaxia.Size = new System.Drawing.Size(161, 32);
            this.textBoxTemaxia.TabIndex = 2;
            this.textBoxTemaxia.TabStop = false;
            this.textBoxTemaxia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 46);
            this.label2.TabIndex = 0;
            this.label2.Text = "Τεμάχια";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(174, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 46);
            this.label3.TabIndex = 1;
            this.label3.Text = "Είδη";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.button2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.button3, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(1159, 22);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(177, 86);
            this.tableLayoutPanel4.TabIndex = 13;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.button2.Location = new System.Drawing.Point(3, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(171, 37);
            this.button2.TabIndex = 9;
            this.button2.Text = "Επιλογή Όλων";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Font = new System.Drawing.Font("Lucida Sans Unicode", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.button3.Location = new System.Drawing.Point(3, 46);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 37);
            this.button3.TabIndex = 10;
            this.button3.Text = "Απεπιλογή Όλων";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // backgroundWorkerNameFinder
            // 
            this.backgroundWorkerNameFinder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerNameFinder_DoWork);
            this.backgroundWorkerNameFinder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerNameFinder_RunWorkerCompleted);
            // 
            // additionTimesBindingSource
            // 
            this.additionTimesBindingSource.DataMember = "AdditionTimes";
            this.additionTimesBindingSource.DataSource = this.orderedProductBindingSource;
            // 
            // dataGridViewComboBoxColumn1
            // 
            this.dataGridViewComboBoxColumn1.DataPropertyName = "Category";
            this.dataGridViewComboBoxColumn1.HeaderText = "ΚΑΤΗΓΟΡΙΑ";
            this.dataGridViewComboBoxColumn1.Name = "dataGridViewComboBoxColumn1";
            this.dataGridViewComboBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewComboBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewComboBoxColumn1.Width = 122;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Category";
            this.dataGridViewTextBoxColumn1.HeaderText = "ΚΑΤΗΓΟΡΙΑ";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.Width = 122;
            // 
            // categoryBindingSource
            // 
            this.categoryBindingSource.DataSource = typeof(Orders.Classes.Category);
            // 
            // orderableProductBindingSource
            // 
            this.orderableProductBindingSource.DataSource = typeof(Orders.DatabaseProduct);
            // 
            // orderableProductBindingSource1
            // 
            this.orderableProductBindingSource1.DataSource = typeof(Orders.DatabaseProduct);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1330, 730);
            this.Controls.Add(this.buttonHidePrinted);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Lucida Sans Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MainForm";
            this.Text = "Ελλείψεις";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderedProductBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.additionTimesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderableProductBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem αποθήκευσηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem εκτύπωσηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem αλλαγήΒάσηςToolStripMenuItem;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Button buttonHidePrinted;
        private System.Windows.Forms.Label labelBarcode;
        private System.ComponentModel.BackgroundWorker backgroundWorkerBarcodeFinder;
        private System.Windows.Forms.BindingSource orderableProductBindingSource;
        private System.Windows.Forms.BindingSource orderableProductBindingSource1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource orderedProductBindingSource;
        private System.ComponentModel.BackgroundWorker backgroundWorkerSave;
        private System.ComponentModel.BackgroundWorker backgroundWorkerLoad;
        private System.Windows.Forms.ToolStripMenuItem φόρτωσηToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ToolStripMenuItem εκτύπωσηΣεΠροεπιλογήToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem εγκατάστασηToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem αποστολήΣεFarmakonToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorkerNameFinder;
        private System.Windows.Forms.ToolStripMenuItem εκτυπωμένεςΠαραγγελίεςToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource additionTimesBindingSource;
        private System.Windows.Forms.TextBox textBoxEidi;
        private System.Windows.Forms.TextBox textBoxTemaxia;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem timeDelayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem διαγραφήΕπιλεγμένωνToolStripMenuItem;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn Category;
        private System.Windows.Forms.DataGridViewTextBoxColumn Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsSelected;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Delete;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxCategory;
    }
}

