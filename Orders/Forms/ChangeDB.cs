﻿using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orders
{
    public partial class ChangeDB : Form
    {
        public ChangeDB()
        {
            InitializeComponent();
        }

        private string dbName, dbCatalogName;

        List<string> SQLServerInstances = new List<string>();
        List<string> SQLServerCatalogs = new List<string>();
        private void ChangeDB_Load(object sender, EventArgs e)
        {
            dbName = Properties.Settings.Default.DatabaseName;
            dbCatalogName = Properties.Settings.Default.DatabaseCatalogName;
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            SqlDataSourceEnumerator instance = SqlDataSourceEnumerator.Instance;
            System.Data.DataTable table = instance.GetDataSources();
            foreach (System.Data.DataRow row in table.Rows)
            {
                SQLServerInstances.Add(row[0].ToString() + @"\" + row[1].ToString());
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            comboBoxInstances.Enabled = true;
            comboBoxInstances.DataSource = SQLServerInstances;
            if (SQLServerInstances.Count <= 0)
            {
                MessageBox.Show("Δεν βρέθηκαν στιγμιότυπα του SQL Server.");
                return;
            }
            if (comboBoxInstances.Items.Count > 0)
                comboBoxInstances.SelectedIndex = 0;
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            string databaseInstanceName = e.Argument.ToString();
            SQLServerCatalogs = GetDatabaseNames(databaseInstanceName);
        }

        private void comboBoxInstances_SelectedValueChanged(object sender, EventArgs e)
        {
            string databaseInstanceName = comboBoxInstances.SelectedItem.ToString();
            //MessageBox.Show("Looking for catalogs in : " + databaseInstanceName);
            try
            {
                backgroundWorker2.RunWorkerAsync(databaseInstanceName);
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Δεν είναι δυνατή η σύνδεση με τον SQL server.", "Σφάλμα");
            }
            catch (SqlException)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα της σύνδεσης με τον SQL Server", "Σφάλμα");
            }
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            comboBoxCatalogs.Enabled = true;
            comboBoxCatalogs.DataSource = SQLServerCatalogs;
        }

        private void comboBoxCatalogs_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Properties.Settings.Default.DatabaseName = comboBoxInstances.SelectedItem.ToString();
            Properties.Settings.Default.DatabaseCatalogName = comboBoxCatalogs.SelectedItem.ToString();
            Properties.Settings.Default.Save();
        }

        public static List<string> GetDatabaseNames(string serverName)
        {
            //string connString = Properties.Settings.Default.testing;
            string connString = "Data Source = " + serverName + ";User Id=" + Properties.Settings.Default.DBUsername + ";Password=" + Properties.Settings.Default.DBPassword + ";";
            List<string> returnValue = new List<string>();
            try
            {
                using (var con = new SqlConnection(connString))
                {
                    con.Open();
                    DataTable databases = con.GetSchema("Databases");
                    foreach (DataRow database in databases.Rows)
                    {
                        returnValue.Add(database.Field<String>("database_name"));
                        //short dbID = database.Field<short>("dbid");
                        //DateTime creationDate = database.Field<DateTime>("create_date");
                    }
                }
            }
            catch (InvalidOperationException)
            {
                MessageBox.Show("Δεν είναι δυνατή η σύνδεση με τον SQL server.", "Σφάλμα");
            }
            catch (SqlException)
            {
                MessageBox.Show("Σφάλμα κατά το άνοιγμα της σύνδεσης με τον SQL Server", "Σφάλμα");
            }
            return returnValue;
            /*
            try
            {
                var server = new Server(serverName);
                return (from Database database in server.Databases
                        select database.Name).ToList();
            }
            catch (Microsoft.SqlServer.Management.Common.ConnectionFailureException e)
            {
                MessageBox.Show("Δεν βρέθηκε το επιλεγμένο στιγμυότυπο", "Σφάλμα");
                return null;
            }*/
        }
    }
}
