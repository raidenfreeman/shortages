﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Orders
{
    public partial class InstallationForm : Form
    {
        public InstallationForm()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            string username = comboBox1.SelectedItem.ToString();
            switch (username)
            {
                case "USER 1":
                    Properties.Settings.Default.UseSQLAuth = true;
                    Properties.Settings.Default.DBUsername = "TAMEIO1";
                    break;
                case "USER 2":
                    Properties.Settings.Default.UseSQLAuth = true;
                    Properties.Settings.Default.DBUsername = "TAMEIO2";
                    break;
                case "SERVER":
                    {
                        Properties.Settings.Default.UseSQLAuth = false;
                        Properties.Settings.Default.DBUsername = "PETROS";
                        break;
                    }
                case "PETROS-PC":
                    {
                        Properties.Settings.Default.UseSQLAuth = false;
                        Properties.Settings.Default.DBUsername = "PETROS";
                        break;
                    }
                case "ΑΠΟΘΗΚΗ":
                    Properties.Settings.Default.UseSQLAuth = true;
                    Properties.Settings.Default.DBUsername = "APOTHIKI";
                    break;
                case "Άλλος":
                    Properties.Settings.Default.UseSQLAuth = true;
                    Properties.Settings.Default.DBUsername = "APOTHIKI";
                    break;
                default:
                    Properties.Settings.Default.UseSQLAuth = true;
                    Properties.Settings.Default.DBUsername = "APOTHIKI";
                    break;
            }
            string domainName = Environment.UserDomainName.ToUpper();
            if (domainName != "SERVER" || domainName != "PETROS-PC" || domainName!= "CORTANAR")
            {
                try
                {
                    System.Data.Sql.SqlDataSourceEnumerator instance = System.Data.Sql.SqlDataSourceEnumerator.Instance;
                    System.Data.DataTable table = instance.GetDataSources();
                    foreach (System.Data.DataRow row in table.Rows)
                    {
                        if (row[1].ToString() == "CSASQL")
                        {
                            Properties.Settings.Default.DatabaseName = row[0].ToString() + @"\" + row[1].ToString();
                            break;
                        }
                    }
                }
                catch(Exception)
                {
                    Properties.Settings.Default.DatabaseName = @"SERVER\CSASQL";
                }
            }
            else
                Properties.Settings.Default.DatabaseName = @"(local)\CSASQL";
            Properties.Settings.Default.Installed = true;
            Properties.Settings.Default.Save();
            MessageBox.Show("Οι ρυθμίσεις ολοκληρώθηκαν επιτυχώς.");
            this.Close();
        }
    }
}
